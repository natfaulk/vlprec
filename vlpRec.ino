#include <Wire.h>
#include "arduinoFFT.h"

void timer_ISR(void);
void printData(void);

const uint8_t ANALOG_PIN = A0;
const uint16_t SAMPLE_RATE = 10000;
const uint16_t NUM_SAMPLES = 1024;
const uint16_t UPDATE_RATE_ms = 250;
const uint32_t BAUD_RATE = 460800;

volatile bool dataFull = false;
volatile uint16_t data[NUM_SAMPLES];
volatile uint16_t dataI = 0;

unsigned long prevTime = 0;

arduinoFFT FFT = arduinoFFT();
double vReal[NUM_SAMPLES];
double vImag[NUM_SAMPLES];

void setup(void)
{
  Serial.begin (BAUD_RATE);
  Serial.flush();
  Serial.println();
  Serial.println("[DEBUG] Setup started");
  timer1_isr_init();
  timer1_attachInterrupt(timer_ISR);
  timer1_enable(TIM_DIV1, TIM_EDGE, TIM_LOOP);
  timer1_write(80000000 / SAMPLE_RATE);
  Serial.println("[DEBUG] Setup finished");

  delay(2000);

  Wire.begin(4, 5);

  byte error, address;
  int nDevices = 0;

  for (address = 1; address < 127; address++ )
  {
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0)
    {
      Serial.print("[DEBUG] I2C device found at address 0x");

      if (address < 16) Serial.print("0");
      Serial.print(address, HEX);
      Serial.println(" !");
      nDevices++;

    }
    else if (error == 4)
    {
      Serial.print("[DEBUG] Unknow error at address 0x");
      if (address < 16) Serial.print("0");
      Serial.println(address, HEX);
    }
  }

  if (nDevices == 0) Serial.print("[DEBUG] No I2C devices found");

  Serial.print("[DEBUG] Address 0x00: ");
  Wire.beginTransmission(0x2E);
  Wire.write(byte(0x0C));
  Wire.requestFrom(0x2E, 2);
  Serial.print("0x");
  while(Wire.available())
  { 
    uint8_t c = Wire.read();
    if (c < 16) Serial.print("0");
    Serial.print(c, HEX);
  }
  Wire.endTransmission();
  Serial.println();

  Serial.print("[DEBUG] Address 0x04: ");  
  Wire.beginTransmission(0x2E);
  Wire.write(byte(0x4C));
  Wire.requestFrom(0x2E, 2);
  Serial.print("0x");
  while(Wire.available())
  { 
    uint8_t c = Wire.read();
    if (c < 16) Serial.print("0");
    Serial.print(c, HEX);
  }
  Wire.endTransmission();
  Serial.println();

  Serial.print("[DEBUG] Address 0x05: ");  
  Wire.beginTransmission(0x2E);
  Wire.write(byte(0x5C));
  Wire.requestFrom(0x2E, 2);
  Serial.print("0x");
  while(Wire.available())
  { 
    uint8_t c = Wire.read();
    if (c < 16) Serial.print("0");
    Serial.print(c, HEX);
  }
  Wire.endTransmission();
  Serial.println();

  Serial.println("[DEBUG] Setting R to max ");
  Wire.beginTransmission(0x2E);
  Wire.write(byte(0x00));
  Wire.write(byte(0x01));
  Wire.endTransmission();

  delay(100);
  
  Serial.print("[DEBUG] Address 0x00: ");
  Wire.beginTransmission(0x2E);
  Wire.write(byte(0x0C));
  Wire.requestFrom(0x2E, 2);
  Serial.print("0x");
  while(Wire.available())
  { 
    uint8_t c = Wire.read();
    if (c < 16) Serial.print("0");
    Serial.print(c, HEX);
  }
  Wire.endTransmission();
  Serial.println();
}

void loop(void)
{
  if (dataFull && (millis() - prevTime) > UPDATE_RATE_ms)
  {
    for (int i = 0; i < NUM_SAMPLES; i++)
    {
      vReal[i] = data[i];
      vImag[i] = 0;
    }

    dataFull = false;


      
    FFT.Windowing(vReal, NUM_SAMPLES, FFT_WIN_TYP_HAMMING, FFT_FORWARD);  /* Weigh data */
    FFT.Compute(vReal, vImag, NUM_SAMPLES, FFT_FORWARD); /* Compute FFT */
    FFT.ComplexToMagnitude(vReal, vImag, NUM_SAMPLES); /* Compute magnitudes */

    printData();
    prevTime = millis();
  }
}

void timer_ISR(void)
{
  if (!dataFull)
  {
    data[dataI++] = analogRead(ANALOG_PIN);
    if (dataI >= NUM_SAMPLES) 
    {
      dataFull = true;
      dataI = 0;
    }
  }
}

void printData(void)
{
  Serial.print("[DEBUG] ADC Data:");
  Serial.print('[');
  for (uint16_t i = 0; i < NUM_SAMPLES; i++)
  {
    Serial.print(vReal[i]);
    if (i < NUM_SAMPLES - 1) Serial.print(',');
    yield();
  }
  Serial.println(']');
}